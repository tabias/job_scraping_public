# -*- coding: utf-8 -*-
"""
Created on Fri Feb 06 11:49:10 2015

@author: Jyldyz
"""
import requests
from lxml import etree
import re
import csv
import pickle
import os

##you may need to change your working directory to the project folder
## change path as needed:
os.chdir("C:/Users/Jyldyz/Documents/Python Scripts/job_scraping")


#get a list of all ONET soc-codes
#the csv file is provided by ONET https://www.onetcenter.org/taxonomy/2010/list.html
taxonomy = open("onet_occupations.csv", "r")

#creates a list of strings for each occupation
occupation_list = []
for t in taxonomy:
    occupation_list.append(t)

#get the soc_code out of string for each occupation and append to a list
#this will later be used to change the link and loop through    
soc_code = []
for l in occupation_list:
    a = l.split(",")
    soc_code.append(a[0])


#get rid of the heading
del soc_code[0]


#accessing html from onetonline.org
fileUrl_base = "http://www.onetonline.org/link/summary/"
wages = []

for ind, c in enumerate(soc_code):
    fileUrl2 = fileUrl_base + c
    r = requests.get(fileUrl2)
    html2 = r.content
    root2 = etree.HTML(html2)

#going deeper into subelements   
    s = root2[1]
    s3 = s[3] #s3[4] contains main info on the occupation

#get median wage info, wage info is stored in td elements
    all_tds = []    
    for ix, content in enumerate(s3.iter("td")): 
        all_tds.append((ix, content.text)) #build list of indices and content
           
    
    for td in all_tds:
        if td[1] == "Median wages (2014)": #text to find, year in the string changes
            wage = all_tds[td[0]+1] #get first value from next list item
            clean_wage = wage[1].split(", ") #drop index and split by ", "
            wages.append([c, clean_wage])
            print ("did one wage", c)
    
    del all_tds[:]

#extract hourly and annual wages using re module
wage_to_code = []
for w in wages:
    single_wage = w[1]
    for sw in single_wage:
        match1 = re.match('(\$[0-9]*\.[0-9]*)\s(hourly)', sw)
        if match1:
            wage_to_code.append([w[0], match1.group(2), match1.group(1)])
        match2 = re.match('(\$[0-9]*\,[0-9]*)\s(annual)', sw)
        if match2:
            wage_to_code.append([w[0], match2.group(2), match2.group(1)])
            
#clean: remove $ and , in thousands separator
clean_wages = []
for wc in wage_to_code:
    nw = wc[2].lstrip('$')
    nw1 = nw.replace(',','')
    clean_wages.append([wc[0], wc[1],nw1])

#convert wage info from string to float
for cw in clean_wages:
    cw[2] = float(cw[2])
    

#write the resuting list to csv for use in Tableau visualization
with open("wages_to_code.csv", "wb") as output:
    f = csv.writer(output, delimiter = ",") 
    f.writerows(clean_wages)
    
#store soc_code for future use
with open("soc_code.pkl", "wb") as outfile:
    pickle.dump(soc_code, outfile)
    
