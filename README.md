# README #

### What is this repository for? ###

_This repository contains scripts for extracting job posting data from Indeed.com and job related information (average salary, related occupations, tools and technology used, skills) from ONET. You can also find here sample output files in csv format (see Output folder). These were generated to create a Tableau Public workbook to visualize and explore collected data._


### How do I get set up? ###

* To access ONET and Indeed.com APIs you will need to register as a developer/partner and obtain authorization details.
* Start with scrape_wages_onet script as one of the script outputs is soc_code list, which is then used by other scripts.
* You can then run onet_titles script. This script builds the dictionary of official and reported job titles for each occupation.
* As a next step, you can run onet_skills and onet_related occupations scripts (order doesn't matter).
* After you're done with ONET data, to get actual job postings run scrape_indeed. Don't forget to specify search terms and location details you're interested in.  
* The title_matching scripts uses the collected Indeed.com data (in csv format) and assigns each actual job title to an official ONET job title. This was done to allow for actual job classification and linking to job metadata (average salary, related occupations, tools and technology used, skills). You may want to manually check title matching in the output (title_to_code2): even though exact matching and fuzzy matching with strict criteria were used, some matching results may be inaccurate. I intend to continue working on the title matching algorithm.

### Contribution guidelines ###

* Any suggestions are welcome, particularly those related to title matching algorithm optimization

### Who do I talk to? ###

* Repo owner or admin