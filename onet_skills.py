# -*- coding: utf-8 -*-
"""
Created on Wed Mar 18 10:15:04 2015

@author: Jyldyz
"""

import requests
from lxml import etree
import csv
import os
import pickle

#you may need to set your working directory to a project folder:
os.chdir("C:/Users/Jyldyz/Documents/Python Scripts/job_scraping")

#open the variations dictionary, which is the dictionary of ONET occupations, where
#dictionary key is ONET soc code and values are known job titles.
with open("variations.pkl", "rb") as infile:
    variations = pickle.load(infile)

#build urls to access info on skills, tools and technology used in a particular occupation
#the link and authorization details are available via ONET API documentation
url_base = "https://services.onetcenter.org/ws/online/occupations/"
#soc_code = "17-2051.00"
skills_url = "/summary/skills"
tools_tech_url = "/details/tools_technology"
#detailed_url = "/details/skills"

#skills segment
#loop through occupations and extract data on most commonly used skills
skills = []
for v in variations:
    job_skill = []
    fileUrl = "".join([url_base, v, skills_url])

    r = requests.get(fileUrl,auth=('user', 'password'))
    xml = r.content
    root = etree.fromstring(xml)

    for element in root.iter("name"):
        job_skill.append(element.text)
    
    skills.append([v, job_skill])
    
#flatten the skills list to write to csv
skills2 = []
for s in skills:
    code = s[0]
    data = s[1]
    for d in data:
        skills2.append([code, d])

#write to csv to use in Tableau
with open("skills2.csv", "wb") as output:
    f = csv.writer(output, delimiter = ",") 
    f.writerows(skills2)                
    
#tools segment
#loop through occupations and extract data on most commonly used tools and examples
#of these tools    
tools_gen = [] #general list with tool names only
tools_example = [] #detailed list with examples of each tool
for v in variations:
    job_tool = []
    fileUrl = "".join([url_base, v, tools_tech_url])
    
    r = requests.get(fileUrl,auth=('exploration', '2894huq'))
    xml1 = r.content
    root1 = etree.fromstring(xml1)
    
    try:
        tools = root1[1] #zoom in on the element that contains tools info
                
        longer = [] #longer list as it will contain both tool names and examples
        short = [] #shorter list with only tool names
        parallel = range(len(tools))
        for ix in parallel:
            if len(tools[ix]) > 1:
                for num in tools[ix].iter("example"):
                    for x in tools[ix].iter("title"):
                        longer.append([x.text, num.text])
                    short.append(x.text)
            if len(tools[ix]) == 1:
                for y in tools[ix].iter("title"):
                    short.append(y.text)
        tools_gen.append([v, list(set(short))]) #list of tool names only
        tools_example.append([v, longer]) #list of tool names and examples
    except:
        print (v, "no data")

#flatten the tool name only list, then write to csv
tools_gen_flat = []
for tg in tools_gen:
    soc_code = tg[0]
    tools = tg[1]
    for t in tools:
        tools_gen_flat.append([soc_code, t])

with open("tools_gen.csv", "wb") as output:
    f = csv.writer(output, delimiter = ",") 
    f.writerows(tools_gen_flat)        

#flatten the tool name & example list, then write to csv
tools_example_flat = []
for te in tools_example:
    soc_code = te[0]
    tools = te[1]
    for t in tools:
        tools_example_flat.append([soc_code, t[0], t[1]])
        
with open("tools_example.csv", "wb") as output:
    f = csv.writer(output, delimiter = ",") 
    f.writerows(tools_example_flat)
        

#technology segment. approach is the same as for tools
#loop through occupations and extract data on most commonly used technology and
#corresponding technology examples
tech_gen = [] #general list with tech names only
tech_example = [] #detailed list with examples of each technology
for v in variations:
    fileUrl = "".join([url_base, v, tools_tech_url])
    
    r = requests.get(fileUrl,auth=('exploration', '2894huq'))
    xml1 = r.content
    root1 = etree.fromstring(xml1)
    
    try:
        tech = root1[2]
        
        longer = []
        short = []
        parallel = range(len(tech))
        for ix in parallel:
            if len(tech[ix]) > 1:
                for num in tech[ix].iter("example"):
                    for x in tech[ix].iter("title"):
                        longer.append([x.text, num.text])
                    short.append(x.text)
            if len(tech[ix]) == 1:
                for y in tech[ix].iter("title"):
                    short.append(y.text)
        tech_gen.append([v, list(set(short))])
        tech_example.append([v, longer])
    except:
        print (v, "no data")
        
    
        
tech_gen_flat = []
for tg in tech_gen:
    soc_code = tg[0]
    tech = tg[1]
    for t in tech:
        tech_gen_flat.append([soc_code, t])
        
with open("tech_gen.csv", "wb") as output:
    f = csv.writer(output, delimiter = ",") 
    f.writerows(tech_gen_flat)
  

tech_example_flat = []
for te in tech_example:
    soc_code = te[0]
    tech = te[1]
    for t in tech:
        tech_example_flat.append([soc_code, t[0], t[1]])
        
with open("tech_example.csv", "wb") as output:
    f = csv.writer(output, delimiter = ",") 
    f.writerows(tech_example_flat)